"use strict";
angular.module("treeViewerApp", []).controller("MainCtrl", ["$scope", "treeService", function (a, b) {
  a.tree = b.getTreeData()
}]), angular.module("treeViewerApp").service("treeService", ["$http", function (a) {
  this.getTreeData = function () {
    var b = {};
    return a.get("content/test1.json").success(function (a) {
      b.data = a
    }), b
  }
}]), angular.module("treeViewerApp").directive("treeNode", ["$compile", function (a) {
  return {
    template: "<li>{{node.name}}</li>", restrict: "E", replace: !0, scope: {node: "="}, link: function (b, c) {
      var d = b.node.nodes;
      void 0 !== d && d.length > 0 && a('<tree nodes="node.nodes"/>')(b, function (a) {
        c.append(a), c.bind("click", function (b) {
          if (b.stopPropagation(), b.target.firstChild === b.target.lastChild)return !0;
          var c = "none" !== a.css("display");
          c ? a.hide() : a.show()
        })
      })
    }
  }
}]), angular.module("treeViewerApp").directive("tree", function () {
  return {
    template: '<ul><tree-node ng-repeat="node in nodes" node="node"></tree-node></ul>',
    restrict: "E",
    replace: !0,
    scope: {nodes: "="}
  }
});

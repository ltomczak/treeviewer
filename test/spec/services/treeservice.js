'use strict';

/**
 * Test serwisu TreeService.
 */
describe('Service: treeService', function () {

  beforeEach(module('treeViewerApp'));


  var treeService, $httpBackend;

  beforeEach(inject(function (_$httpBackend_, _treeService_) {
    $httpBackend = _$httpBackend_;

    $httpBackend.expectGET('content/test1.json').respond([{}, {}]);

    treeService = _treeService_;
  }));

  it('Should return 2 node objects', function () {
    expect(!!treeService).toBe(true);

    var treeData = treeService.getTreeData();
    $httpBackend.flush();

    expect(treeData).not.toBeUndefined();
    expect(treeData.data).not.toBeUndefined();
    expect(treeData.data.length).toBe(2);
  });

});

'use strict';

/**
 * Test głównego kontrolera.
 */
describe('Controller: MainCtrl', function () {

  var mockTreeService = {
    getTreeData: function () {
      return {
        data: [{}, {}]
      };
    }
  };

  beforeEach(module('treeViewerApp', function ($provide) {
    $provide.value('treeService', mockTreeService);
  }));

  var MainCtrl, scope;
  beforeEach(inject(function ($controller, $rootScope) {

    scope = $rootScope.$new();

    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('Should attach a list of 2 nodes to the scope', function () {
    expect(scope.tree).not.toBeUndefined();
    expect(scope.tree.data.length).toBe(2);
  });
});


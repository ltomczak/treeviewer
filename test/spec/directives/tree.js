'use strict';

/**
 * Test dyrektywy tree.
 */
describe('Directive: tree', function () {

  beforeEach(module('treeViewerApp'));

  var element, scope;

  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();

    element = angular.element('<tree nodes="tree"></tree>');
    element = $compile(element)(scope);
  }));

  it('should have two elements without childrens', inject(function () {
    scope.tree = [{name: '1'}, {name: '2'}];
    scope.$digest();

    var liElems = element.find('li');
    expect(liElems.length).toBe(2);

    var liElem1 = angular.element(liElems[0]), liElem2 = angular.element(liElems[1]);
    expect(liElem1.text()).toBe('1');
    expect(liElem2.text()).toBe('2');

    expect(angular.element(liElems[0]).children().length).toBe(0);
    expect(angular.element(liElems[1]).children().length).toBe(0);
  }));

  it('should have one element with two childrens', inject(function () {
    scope.tree = [{name: '1', nodes: [{name: '2'}, {name: '3'}]}];
    scope.$digest();

    var liElems = element.find('li');
    expect(liElems.length).toBe(3);

    var liElem1 = angular.element(liElems[0]), liElem2 = angular.element(liElems[1]), liElem3 = angular.element(liElems[2]);
    expect(liElem1.text()).toBe('123');
    expect(liElem2.text()).toBe('2');
    expect(liElem3.text()).toBe('3');

    var ulChildElem = liElem1.find('ul');
    expect(ulChildElem.length).toBe(1);
    expect(angular.element(ulChildElem).children().length).toBe(2);
  }));

  it('click should hide childrens', inject(function () {
    scope.tree = [{name: '1', nodes: [{name: '2'}, {name: '3'}]}];
    scope.$digest();

    var firstLi = angular.element(element.find('li')[0]),
      ulElem = firstLi.find('ul');

    expect(ulElem.attr('style')).toBeUndefined();
    firstLi.click();
    expect(ulElem).toBeHidden();
  }));

});

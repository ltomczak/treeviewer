'use strict';

/**
 * Główny kontroler aplikacji. Dodatkowo tworzony moduł o nazwie: treeViewerApp.
 *
 * @ngdoc function
 * @name treeViewerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the treeViewerApp
 */
angular.module('treeViewerApp', [])
  .controller('MainCtrl', function ($scope, treeService) {
    $scope.tree = treeService.getTreeData();
  });

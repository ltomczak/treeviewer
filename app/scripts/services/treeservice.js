'use strict';

/**
 * Serwis pobiera dane drzewa.
 *
 * @ngdoc service
 * @name treeViewerApp.treeService
 * @description
 * # treeService
 */
angular.module('treeViewerApp')
  .service('treeService', function ($http) {

    /**
     * Pobiera dane drzewa.
     * @return obiekt z danymi drzewa znajdującymi się w atrybucie data
     */
    this.getTreeData = function () {
      var treeData = {};

      $http.get('content/test1.json').success(function (data) {
        treeData.data = data;
      });

      return treeData;
    };

  });



'use strict';

/**
 * Dyrektywa budująca drzewo na podstawie dostarczonej struktury za pomocą standardowych znaczników listy HTML.
 * Wykorzystuje dyrektywę pomocniczą tree-node do wywołań rekurencyjnych.
 *
 * @ngdoc directive
 * @name treeViewerApp.directive:tree
 * @description
 * # tree
 */
angular.module('treeViewerApp')
  .directive('tree', function () {
    return {
      template: '<ul><tree-node ng-repeat="node in nodes" node="node"></tree-node></ul>',
      restrict: 'E',
      replace: true,
      scope: {
        nodes: '='
      }
    };
  });

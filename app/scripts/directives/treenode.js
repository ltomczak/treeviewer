'use strict';

/**
 * Dyrektywa pomocnicza dla dyrektywy tree - wywołanie rekurencyjne.
 *
 * @ngdoc directive
 * @name treeViewerApp.directive:treeNode
 * @description
 * # treeNode
 */
angular.module('treeViewerApp')
  .directive('treeNode', function ($compile) {
    return {
      template: '<li>{{node.name}}</li>',
      restrict: 'E',
      replace: true,
      scope: {
        node: '='
      },
      link: function link(scope, element, attrs) {
        var nodes = scope.node.nodes;

        //jeżeli są elementy dzieci
        if (nodes !== undefined && nodes.length > 0) {
          $compile('<tree nodes="node.nodes"/>')(scope, function (cloned, scope) {
            element.append(cloned);


            element.bind('click', function (event) {
              event.stopPropagation();
              if (event.target.firstChild === event.target.lastChild) return true;

              var isVisible = cloned.css('display') !== 'none';

              if (!isVisible) cloned.show();
              else cloned.hide();
            });
          });
        }
      }
    };
  });

# README #

Zbudowana wersja aplikacji w katalogu build. W przypadku uruchamiania pod przeglądarką Chrome (lub innych opartych na silniku Webkit) należy uruchomić ją z przełącznikiem "--allow-file-access-from-files".

Do zbudowania aplikacji wymagane są narzędzia npm, bower i grunt-cli.
 
* budowanie: grunt build
* uruchomienie na lokalnym serwerze node: grunt serve
* uruchomienie testów: grunt test